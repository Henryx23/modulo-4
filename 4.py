placa = ""
while True:
    placa = input("Introduzca la placa: ")
    if len(placa) != 5 or not placa.isdigit():
        print("La placa no es válida")
    else:
        break
primer_digito = placa[0]
tipo_vehiculo = "Se desconoce el tipo de vehiculo"
if primer_digito == "1":
    tipo_vehiculo = "Kia"
elif primer_digito == "2":
    tipo_vehiculo = "Hyundai"
elif primer_digito == "3":
    tipo_vehiculo = "Honda"
print("La placa es de un vehículo " + tipo_vehiculo)
